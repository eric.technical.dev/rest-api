package fr.erdprt.api.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import fr.erdprt.Application;
import fr.erdprt.api.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
//@WebMvcTest(UserController.class)
public class UserControllerTest {

	@Autowired
	private UserController userController;

	@Test 
	public void findById() {
		ResponseEntity<User> user =  userController.findById(1);
		Assert.assertNotNull(user);
		Assert.assertNotNull(user.getBody());
	}
}
