package fr.erdprt.api.dao;

import org.springframework.data.repository.CrudRepository;

import fr.erdprt.api.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
}
