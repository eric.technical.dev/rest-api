package fr.erdprt.api.dao;

import org.springframework.data.repository.CrudRepository;

import fr.erdprt.api.model.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {
}
