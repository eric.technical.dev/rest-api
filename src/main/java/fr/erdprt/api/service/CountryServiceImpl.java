package fr.erdprt.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import fr.erdprt.api.dao.CountryRepository;
import fr.erdprt.api.model.Country;

@CacheConfig(cacheNames = { "country" })
@Service("countryService")
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepository;

	@Override
	public List<Country> findAll() {

		List<Country> list = new ArrayList<>();
		this.countryRepository.findAll().forEach(list::add);
		return list;
	}

	@Cacheable(value = "country", key = "#id")
	@Override
	public Country findById(final Integer id) {
		Optional<Country> country = this.countryRepository.findById(id);
		if (!country.isEmpty()) {
			return country.get();
		}
		return null;
	}

	@Cacheable(value = "country", key = "#id")
	@Override
	public Country save(final Country country) {
		return this.countryRepository.save(country);
	}
}
