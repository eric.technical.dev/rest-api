package fr.erdprt.api.service;

import java.util.List;

import fr.erdprt.api.model.Country;

public interface CountryService {

	List<Country> findAll();

	Country findById(Integer id);

	Country save(Country country);

}