package fr.erdprt.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import fr.erdprt.api.dao.UserRepository;
import fr.erdprt.api.model.User;

@CacheConfig(cacheNames={"user"})  
@Service("userService")
public class UserServiceImpl implements UserService  {

	@Autowired
	private UserRepository userRepository;
	
	
	@Override
	public List<User> findAll() {
		List<User> list = new ArrayList<User>();
		this.userRepository.findAll().forEach(list::add);
		return list;
	}
	
	@Override
	@Cacheable(value="user", key="#id")
	public User findById(final Integer id) {
		Optional<User> user =  this.userRepository.findById(id);
		if (!user.isEmpty()) {
			return user.get();
		}
		return null;
	}

	@Override
	@Cacheable(value="user", key="#id")
	public User save(final User user) {
		return this.userRepository.save(user);
	}
}
