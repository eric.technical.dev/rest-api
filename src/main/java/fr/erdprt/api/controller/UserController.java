package fr.erdprt.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.erdprt.api.model.User;
import fr.erdprt.api.service.UserService;
import fr.erdprt.api.util.CustomErrorType;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping(value = "/api/users", produces = "application/json")
public class UserController {

	public static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserService userService;

	// -------------------Retrieve All country---------------------------------------------

	@GetMapping(value = "/all/")
	public ResponseEntity<List<User>> findAll() {
		logger.info("Fetching all users");
		List<User> users = userService.findAll();
		/*
		if (countries.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		*/
		return new ResponseEntity<>(users, HttpStatus.OK);
	}

	// -------------------Retrieve Single country------------------------------------------

	@GetMapping(value = "/{id}")
	public ResponseEntity<User> findById(@PathVariable("id") Integer id) {
		logger.info("Fetching country with id {}", id);
		User user = userService.findById(id);
		if (id == 0L) {
			throw new RuntimeException("0 is a bad id");
		}
		if (user == null) {
			logger.error("User with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("user with id " + id + " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	// -------------------Create a country-------------------------------------------
	@PostMapping(value = "/")
	public ResponseEntity<User> save(@RequestBody User user) {
		logger.info("Save user : {}", user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

}