package fr.erdprt.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import fr.erdprt.api.model.Country;
import fr.erdprt.api.service.CountryService;
import fr.erdprt.api.util.CustomErrorType;


@RestController
@RequestMapping(value = "/api/countries", produces = "application/json")
public class CountryController {

	public static final Logger logger = LoggerFactory.getLogger(CountryController.class);

	@Autowired
	CountryService countryService;

	// -------------------Retrieve All country---------------------------------------------

	@GetMapping(value = "/all/")
	public ResponseEntity<List<Country>> findAll() {
		logger.info("Fetching all countries");
		List<Country> countries = countryService.findAll();
		return new ResponseEntity<>(countries, HttpStatus.OK);
	}

	// -------------------Retrieve Single country------------------------------------------

	@GetMapping(value = "/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
		logger.info("Fetching country with id {}", id);
		Country country = countryService.findById(id);
		if (id == 0L) {
			throw new RuntimeException("0 is a bad id");
		}
		if (country == null) {
			logger.error("Country with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Country with id " + id + " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(country, HttpStatus.OK);
	}

	// -------------------Create a country-------------------------------------------
	@PostMapping(value = "/")
	public ResponseEntity<Country> save(@RequestBody Country country) {
		logger.info("Save country : {}", country);
		return new ResponseEntity<>(country, HttpStatus.OK);
	}
}