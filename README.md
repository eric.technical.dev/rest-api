Rest Application sample

spring boot sample
=================
# Table of contents

1. [Introduction](#1-introduction)
2. [Actuator](#2-actuator)
  1. [Urls](#21-urls)


# 1. Introduction

Run with:
```bash
mvn sprint-boot:run
```

Access to :
```bash
http://localhost:8080/rest-api/api/name/
```

## 1.1. endpoints

### 1.1.1 console h2
http://localhost:8080/rest-api/h2-console


### 1.1.2 countries
http://localhost:8080/rest-api/h2-console

# 2. Actuator

## 21. Urls

health:
```bash
http://localhost:8080/rest-api/actuator/health
```

info:
```bash
http://localhost:8080/rest-api/actuator/info
```

metrics:
```bash
http://localhost:8080/rest-api/actuator/metrics
```

loggers:
```bash
http://localhost:8080/rest-api/actuator/loggers
```

# 3. Docker

## 31. Build
```bash
docker build -t rest-api .
```


## 32. Run
```bash
docker run -d -p 8080:8080 --name rest-api rest-api
```

## 33. pull from gitlab registry
```bash
docker pull registry.gitlab.com/erdprt/rest-api:dev-rest-api-1.0.0-SNAPSHOT
```

# 4. SonarCloud

## 31. Access
```bash
https://sonarcloud.io/project/overview?id=erdprt_rest-api
```
